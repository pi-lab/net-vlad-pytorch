# NetVLAD - PyTorch

NetVLAD是视觉场景识别定位的经典之作，熟悉其原理和实现对开发视觉定位项目十分重要。本项目包括了PyTorch版本的NetVLAD实现，根据本项目可以从零开始实现NetVLAD
![](./fig/netvlad.jpg)
## 0. 前置知识

>Note: 该部分内容主要针对零基础人员理解深度学习基本原理并上手该项目，如果你已经具备一定的深度学习基础(如：了解YOLO等目标检测原理或FastRCNN相关图像分割实现)或动手实现、复现过一个深度学习视觉项目，该部分可以直接掉过。参考最后链接，阅读NetVLAD原文及相关博客，然后运行代码即可。
>

图像包含了丰富的特征，简单来说包括颜色、图像中的人物、标志性建筑、明亮度等等。人们能够从这些特征中提取并分析得到需要信息，进而衍生出很多相关应用，如：目标识别、图像分割、行人重识别、差异检测等。视觉场景识别也是其中一个研究热点，其原理可以简要概括为识别图像特征并根据特征判断图像拍摄位置。快速理解其内涵需要搞清楚以下两个问题。

+ [卷积神经网络如何通过卷积操作提取图像特征](https://zhuanlan.zhihu.com/p/31657315)
+ [根据提取的特征如何分析出所需信息——深度学习的工作原理](https://blog.csdn.net/github_39421713/article/details/86546229)

如果时间充足，可以系统学习深度学习相关课程和书本，参考布树辉教授[《机器学习算法与实现-Python编程与应用实例》](http://www.adv-ci.com/blog/books/)，以及[配套视频课程](https://www.bilibili.com/video/BV1oZ4y1N7ei/)，有助于由浅到深学习机器学习核心原理。

## 1. Setup

### 1.1 Dependencies

首先需要安装如下依赖包，最好在conda环境下配置

1. [PyTorch](https://pytorch.org/get-started/locally/) (at least v0.4.0)
2. [Faiss](https://github.com/facebookresearch/faiss)
3. [scipy](https://www.scipy.org/)
    - [numpy](http://www.numpy.org/)
    - [sklearn](https://scikit-learn.org/stable/)
    - [h5py](https://www.h5py.org/)
4. [tensorboardX](https://github.com/lanpa/tensorboardX)

安装的命令：

```bash
conda create -n NetVlad python=3.7
conda activate NetVlad
conda install pytorch==1.11.0 torchvision==0.12.0 torchaudio==0.11.0 -c pytorch
conda install numpy scikit-learn 
pip insyall tensorboardX
pip install faiss-gpu
conda install scipy
```

### 1.2 数据集

内部地址： /home/a409/research_data/cv/netvlad_v100_datasets/

百度云链接：https://pan.baidu.com/s/1EfLV1uwTGLUvpCDgXBZBow?pwd=wqyq 
提取码：wqyq 


## 2. 执行程序

为了安全起见，请先fork，再将fork后的代码clone到本地自己的文件夹运行和更改程序，示例如下：

```
git clone git@gitee.com:@YOUR_NAME/net-vlad-pytorch.git
```

本示例为运行Pittsburgh数据集范例

### 2.1 Prepare

+ 首先需要首先将`main.py`中的下列文件路径更该文自己本地的路径，下面为需要修改的路径，以及对应说明
  + `runsPath` ：储存训练数据、结果，如loss数据等
  + `savePath` ：储存训练好的模型参数
  + `cachePath` ：训练中间数据
  + `dataPath` ：储存cluster后的数据
  + 示例:
  
  ```python
  parser.add_argument('--dataPath', type=str, default='/home/a409/users/name/Projects/pytorch-NetVlad/centroids/', help='Path for centroid data.')
  parser.add_argument('--runsPath', type=str, default='/home/a409/users/name/Projects/2022_code/pytorch-NetVlad/runs/', help='Path to save runs to.')
  parser.add_argument('--savePath', type=str, default='checkpoints', 
        help='Path to save checkpoints to in logdir. Default=checkpoints/')
  parser.add_argument('--cachePath', type=str, default='/home/a409/users/name/Projects/2022_code/pytorch-NetVlad/TMPDIR/')
  ```
  
+ 更改 `pittsburgh.py` 中数据路径

	```python
	# 原始数据存储路径
	root_dir = '/home/a409/research_data/cv/netvlad_v100_datasets/'
	```

### 2.2 Cluster

运行训练代码前首先运行cluster程序，cluster的结果保存在dataPath路径下

```
python main.py --mode=cluster --arch=vgg16 --pooling=netvlad --num_clusters=64
```

### 2.3 Train

```shell
python main.py --mode=train --arch=vgg16 --pooling=netvlad --num_clusters=64
```

提示：加载缓冲数据需要较长时间，这部分代码中未优化

### 2.4 Test

    python main.py --mode=test --resume=runsPath/Nov19_12-00-00_vgg16_netvlad --split=test

## 3. Result

+ 文章中的实验结果如下

|                                                   | R@1  | R@5  | R@10 |
| ------------------------------------------------- | ---- | ---- | ---- |
| [NetVlad paper](https://arxiv.org/abs/1511.07247) | 84.1 | 94.6 | 95.5 |
| pytorch-NetVlad(alexnet)                          | 68.6 | 84.6 | 89.3 |
| pytorch-NetVlad(vgg16)                            | 85.2 | 94.8 | 97.0 |


## 4. 参考资料

+ [NetVLAD论文](https://arxiv.org/abs/1511.07247)
+ [其他衍生版本及参考文献](https://www.cnblogs.com/salt0107fish/p/16046815.html)
